import numpy as np
import cv2

imagem = cv2.imread("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/entrada/exercicio02.jpg")

colagem = imagem.shape
altura, largura = imagem.shape[:2]

deslocamento = np.float32([[1, 0, 0], [0, 1, 200]])
deslocado = cv2.warpAffine(imagem, deslocamento, (largura, altura))
cv2.imwrite("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/saida/exercicio02-y.jpg", deslocado)

deslocamento = np.float32([[1, 0, 220], [0, 1, 0]])
deslocado = cv2.warpAffine(imagem, deslocamento, (largura, altura))
cv2.imwrite("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/saida/exercicio02-x.jpg", deslocado)

deslocamento = np.float32([[1, 0, 220], [0, 1, 200]])
deslocado = cv2.warpAffine(imagem, deslocamento, (largura, altura))
cv2.imwrite("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/saida/exercicio02-x-e-y.jpg", deslocado)