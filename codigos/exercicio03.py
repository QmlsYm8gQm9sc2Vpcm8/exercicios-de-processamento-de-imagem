import cv2
import numpy as np

imagem = cv2.imread("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/entrada/exercicio03.jpg")

colagem = imagem.shape
altura, largura = imagem.shape[:2]

ponto = (largura/2, altura/2)

rotacao = cv2.getRotationMatrix2D(ponto, 180, 1)
rotacionando = cv2.warpAffine(imagem, rotacao, (largura, altura))

cv2.imwrite("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/saida/exercicio03-01.jpg", rotacionando)