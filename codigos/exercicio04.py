import numpy as np
import cv2

imagem = cv2.imread("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/entrada/exercicio04.jpg")


#Horizontal
inverter = cv2.flip(imagem, 1)
cv2.imwrite("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/saida/exercicio04-horizontal.jpg", inverter)

#Vertical
inverter = cv2.flip(imagem, -1)
cv2.imwrite("/home/keven/Desktop/exercicio-processamento-de-imagem/imagens/saida/exercicio04-vertical.jpg", inverter)